function FindHospitalItem(props) {
  const { id, hospitalName, email, phone, address } = props.data;

  return (
    <div className="col">
      <div className="card hospital-card">
        <figure>
          <img src="./hospital3.jpg" alt="hospital" />
        </figure>
        <div className="detail">
          <h5 className="hospital-name mb-sm-6">{ hospitalName }</h5>
          <div className="row hospital-info">
            <div className="col-lg-6 px-0 py-1">
              <div className="hidden-xs-down">
                <i className="fa fa-phone me-2"></i>{ phone }
              </div>
            </div>
            <div className="col-lg-6 px-0 py-1">
              <div className="hidden-xs-down">
                <i className="far fa-envelope me-2"></i>{ email }
              </div>
            </div>
            <div className="col-md-12 px-sm-2 px-0 py-1">
              <div className="text-truncate">
                <i className="fas fa-map-marker-alt me-2"></i>{ address }
              </div>
            </div>
          </div>
          <div className="hidden-xs-down text-end mt-4">
            <a className="btn btn-outline-primary rounded-pill px-5" href="tel:0898968765">
              Telepon
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FindHospitalItem;
