import FindHospitalList from "./FindHospitalList";
import "./FindHospital.css";
import { useState, useEffect } from "react";
import axios from "axios";
import { baseUrl } from "../../utils/constant";

function FindHospital() {
  const [filter, setFilter] = useState("");
  const [hospitals, setHospitals] = useState([]);

  const filterChangeHandler = (e) => {
    setFilter(e.target.value);
  };
  

  useEffect(() => {
    const url = `${baseUrl}/hospital-list`;
    axios.get(url).then((res) => {
      setHospitals(res.data.hospitals);
    });
  }, []);

  return (
    <section id="cari-rumah-sakit" className="d-flex align-items-center">
      <div className="container">
        <h1 className="heading-title">Cari Rumah Sakit</h1>
        <div className="heading-subtitle">Termukan Rumah Sakit disekitar anda</div>
        <select className="form-select mb-4" aria-label="Default select example" required style={{ width: "300px" }} defaultValue="" onChange={filterChangeHandler}>
          <option value="">Semua</option>
          <option value="JAKARTA">Jakarta</option>
          <option value="MEDAN">Medan</option>
        </select>
        <FindHospitalList filter={filter} data={hospitals}/>
      </div>
    </section>
  );
}

export default FindHospital;
