import FindHospitalItem from "./FindHospitalItem";

function FindHospitalList(props) {
  const filteredData = props.filter ? props.data.filter((f) => f.city === props.filter) : props.data;
  return (
    <div className="row row-cols-1 row-cols-lg-2">
      {filteredData.map((item) => (
        <FindHospitalItem data={item} key={item.id} />
      ))}
    </div>
  );
}

export default FindHospitalList;
