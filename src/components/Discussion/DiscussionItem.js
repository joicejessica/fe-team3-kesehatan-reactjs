import { useState } from "react";
import DiscussionDetailModal from "./DiscussionDetailModal";

function DiscussionItem(props) {
  const [showModal, setShowModal] = useState(false);

  const showModalHandler = () => setShowModal(true);
  const hideModalHandler = () => setShowModal(false);

  const { title, question, sender, doctor, answer } = props.data;
  const shortText = question.length < 115 ? question : question.substring(0, 115) + "...";

  return (
    <>
      <div className="question">
        <div className="question-header">
          <div className="question-avatar">
            <div id="profileAvatar" className="profile-avatar">
              A
            </div>
            <img id="profileAvatarDoctor" className="profile-avatar-doctor" alt="dr. Nadia Nurotul Fuadah" src="./doc1.webp" />
          </div>
          <div className="question-meta">
            <div className="question-title">{title}</div>
            <div className="question-sender">Oleh: {sender}</div>
            <div className="question-doctor"> Dijawab: {doctor}</div>
            <div className="question-body">{shortText}</div>
            <button onClick={showModalHandler} type="button" className="btn btn-link read-more">
              Baca Selengkapnya
            </button>
          </div>
        </div>
      </div>
      <DiscussionDetailModal show={showModal} onHide={hideModalHandler} data={props.data} />
    </>
  );
}

export default DiscussionItem;
