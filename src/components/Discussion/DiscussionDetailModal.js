import Modal from "react-bootstrap/Modal";

function DiscussionDetailModal(props) {
  const { title, question, sender, doctor, answer } = props.data;

  return (
    <Modal show={props.show} onHide={props.onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Diskusi</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="question-title-header">{ title }</div>

        <div className="questions">
          <div className="question">
            <div className="question-header">
              <div className="question-avatar">
                <div id="profileAvatar" className="profile-avatar" style={{ backgroundColor: "rgb(92 176 77)" }}>
                  A
                </div>
              </div>
              <div className="question-meta">
                <div className="question-title">{ sender }</div>
                <div className="question-body">{ question }</div>
              </div>
            </div>

            <div className="question-header answer">
              <div className="question-avatar">
                <img id="profileAvatarDoctor" className="profile-avatar" alt="dr. Nadia Nurotul Fuadah" src="./doc1.webp" />
              </div>
              <div className="question-meta">
                <div className="question-title">{ doctor }</div>
                <div className="question-body"> { answer }
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default DiscussionDetailModal;
