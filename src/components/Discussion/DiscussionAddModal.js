import Modal from "react-bootstrap/Modal";

function DiscussionAddModal(props) {

  return (
    <Modal show={props.show} onHide={props.onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Tanya Dokter</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form id="formQuestion" onSubmit={props.onSubmit}>
          <div className="modal-body px-5">
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Topik
              </label>
              <select className="form-select" aria-label="Default select example" required>
                <option value="1">Kesehatan</option>
                <option value="2">Covid</option>
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlTextarea1" className="form-label">
                Judul
              </label>
              <input type="text" className="form-control" required />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlTextarea1" className="form-label">
                Deskripsi
              </label>
              <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button className="btn btn-primary" type="submit">
              Kirim
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
}

export default DiscussionAddModal;
