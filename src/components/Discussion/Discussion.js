import { useState, useEffect } from "react";
import DiscussionAddModal from "./DiscussionAddModal";
import DiscussionList from "./DiscussionList";
import "./Discussion.css";

function Discussion(props) {
  const [showModal, setShowModal] = useState(false);

  const showModalHandler = () => setShowModal(true);
  const hideModalHandler = () => setShowModal(false);

  const submitHandler = (e) => {
    e.preventDefault();
    setShowModal(false);
  };

  return (
    <>
      <section id="tanya-dokter" className="d-flex align-items-center">
        <div className="container">
          <h1 className="heading-title">Tanya Dokter</h1>
          <div className="heading-subtitle">Tanyakan masalah kesehatan anda dengan dokter kami</div>
          {props.isLoggedIn ? (
            <button className="btn btn-outline-primary rounded-pill px-5 float-right mb-5" type="button" onClick={showModalHandler}>
              <i className="fas fa-plus"></i> Buat Pertanyaan
            </button>
          ) : (
            <span></span>
          )}
          <DiscussionList />
        </div>
      </section>
      <DiscussionAddModal show={showModal} onHide={hideModalHandler} onSubmit={submitHandler} />
    </>
  );
}

export default Discussion;
