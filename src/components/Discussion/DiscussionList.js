import DiscussionItem from "./DiscussionItem";
import data from "../../data/discussion.json";

function DiscussionList() {
  return (
    <div className="questions">
      {data.map((item) => (
        <DiscussionItem key={item.id} data={item} />
      ))}
    </div>
  );
}

export default DiscussionList;
