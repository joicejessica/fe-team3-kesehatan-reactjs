import "./Footer.css";

function Footer() {
  return (
    <footer id="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="contact-about">
              <div className="contact-logo d-flex align-items-center">
                <img src="./logo.png" alt="logo" />
                <h3 className="mb-0 ms-3">Ruang Sehat</h3>
              </div>
              <p>Ruang Sehat merupakan media yang memudahkan pengguna untuk melakukan konsultasi serta mendapatkan informasi kesehatan terkini</p>
              <div>&copy; Ruang Sehat 2022</div>
            </div>
          </div>
          <div className="col-lg-2">
            <h4>Halaman</h4>
            <ul className="footer-sitemap">
              <li>Beranda</li>
              <li>Tanya Dokter</li>
              <li>Info Kesehatan</li>
              <li>Cari Rumah Sakit</li>
            </ul>
          </div>
          <div className="col-lg-2">
            <h4>Lainnya</h4>
            <ul className="footer-sitemap">
              <li>Syarat dan Ketentuan</li>
              <li>Privasi</li>
            </ul>
          </div>
          <div className="col-lg-4 col-md-6 mt-4 mt-md-0 text-end">
            <div className="social-links">
              <a href="/#" className="twitter">
                <i className="fab fa-twitter"></i>
              </a>
              <a href="/#" className="facebook">
                <i className="fab fa-facebook"></i>
              </a>
              <a href="/#" className="instagram">
                <i className="fab fa-instagram"></i>
              </a>
              <a href="/#" className="linkedin">
                <i className="fab fa-youtube"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
