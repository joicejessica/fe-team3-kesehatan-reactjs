import "./Banner.css";

function Banner() {
  return (
    <section id="banner" className="d-flex align-items-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
            <h1 data-aos="fade-up">RS Dengan BPJS? Temukan Sekarang Juga!</h1>
            <h2 data-aos="fade-up" data-aos-delay="400">
              Cari Rumah Sakit terdekat dengan layanan BPJS kini hanya dengan seujung jari!
            </h2>
            <div data-aos="fade-up" data-aos-delay="800">
              <a href="#cari-rumah-sakit" className="btn btn-primary btn-lg rounded-pill px-5">
                Lebih Lanjut
              </a>
            </div>
          </div>
          <div className="col-lg-6 order-1 order-lg-2 hero-img">
            <img src="./banner.png" className="img-fluid" alt="banner" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Banner;
