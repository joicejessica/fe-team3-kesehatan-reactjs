import FindDoctorList from "./FindDoctorList";
import "./FindDoctor.css";
import { useState } from "react";

function FindDoctor() {
  const [filter, setFilter] = useState("");

  const filterChangeHandler = (e) => {
    setFilter(e.target.value);
  };

  return (
    <section id="cari-dokter" className="d-flex align-items-center">
      <div className="container">
        <h1 className="heading-title">Cari Dokter</h1>
        <div className="heading-subtitle">Termukan Dokter disekitar anda</div>
        <select className="form-select mb-4" aria-label="Default select example" required style={{ width: "300px" }} defaultValue="" onChange={filterChangeHandler}>
          <option value="">Semua</option>
          <option value="THT">Dokter THT</option>
          <option value="KANDUNGAN">Dokter Kandungan</option>
          <option value="ANAK">Dokter Anak</option>
        </select>
        <FindDoctorList filter={filter}/>
      </div>
    </section>
  );
}

export default FindDoctor;
