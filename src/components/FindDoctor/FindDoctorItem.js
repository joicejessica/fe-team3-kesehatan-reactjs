function FindDoctorItem(props) {
  const { name, phone, total, address } = props.data;
  return (
    <div className="col">
      <div className="card doctor-card">
        <figure>
          <img src="./doctor1.jpg" alt="doctor" className="img-fluid" />
        </figure>
        <div className="detail">
          <h5 className="doctor-name mb-sm-6">{ name }</h5>
          <div className="row doctor-info">
            <div className="col-lg-6 px-0 py-1">
              <div className="hidden-xs-down">
                <i className="fa fa-phone me-2"></i>{ phone }
              </div>
            </div>
            <div className="col-lg-6 px-0 py-1">
              <div className="hidden-xs-down">
                <i className="fas fa-hospital-user me-2"></i>{ total }
              </div>
            </div>
            <div className="col-md-12 px-sm-2 px-0 py-1">
              <div className="text-truncate">
                <i className="fas fa-map-marker-alt me-2"></i>{ address }
              </div>
            </div>
          </div>
          <div className="hidden-xs-down text-end mt-4">
            <a className="btn btn-outline-primary rounded-pill px-5" href="tel:0898968765">
              Telepon
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FindDoctorItem;
