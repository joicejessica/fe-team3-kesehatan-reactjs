import FindDoctorItem from "./FindDoctorItem";
import data from "../../data/doctor.json";


function FindDoctorList(props) {
  const filteredData = props.filter ? data.filter((f) => f.specialization === props.filter) : data;

  return (
    <div className="row row-cols-1 row-cols-lg-2">
      {filteredData.map((item) => (
        <FindDoctorItem data={item} key={item.id} />
      ))}
    </div>
  );
}

export default FindDoctorList;
