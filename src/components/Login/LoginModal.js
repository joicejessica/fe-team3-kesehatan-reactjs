import Modal from "react-bootstrap/Modal";
import Alert from "react-bootstrap/Alert";
import "./Login.css";
import { useState } from "react";
import axios from "axios";
import { baseUrl } from "../../utils/constant";

function LoginModal(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showError, setShowError] = useState(false);
  const [message, setMessage] = useState(false);

  const emailChangeHandler = (e) => {
    setEmail(e.target.value);
  };
  const passwordChangeHandler = (e) => {
    setPassword(e.target.value);
  };

  const setLogin = (data) => {
    localStorage.setItem("userInfo", JSON.stringify(data));
  };

  const resetForm = () => {
    setEmail("");
    setPassword("");
  };
  const submitHandler = (e) => {
    e.preventDefault();
    const request = {
      email,
      password,
    };

    const url = `${baseUrl}/login`;
    axios.post(url, request).then((res) => {
      console.log(res);
      if (res.data.login) {
        setMessage("Login Berhasil");
        setLogin(request);
        props.onSuccess();
        setShowError(false);
        resetForm();
      } else {
        setMessage("Login Gagal");
        setShowError(true);
      }
    });
  };

  return (
    <>
      <Modal show={props.show} onHide={props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Masuk</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className="login-form" onSubmit={submitHandler}>
            <div className="icon d-flex align-items-center justify-content-center">
              <span className="fas fa-user"></span>
            </div>
            <div className="form-group mb-3">
              <input type="email" value={email} onChange={emailChangeHandler} className="form-control rounded-left" placeholder="Email" required />
            </div>
            <div className="form-group d-flex mb-5">
              <input type="password" value={password} onChange={passwordChangeHandler} className="form-control rounded-left" placeholder="Password" required />
            </div>
            {showError ? <Alert variant="danger">Username atau password salah</Alert> : <span></span>}
            <div className="form-group mb-3">
              <button type="submit" className="btn btn-primary w-100 px-5">
                Masuk
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default LoginModal;
