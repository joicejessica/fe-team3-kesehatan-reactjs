import HealthInfoItem from "./HealthInfoItem";

function HealthInfoList(props) {
  return (
    <div className="row row-cols-1 row-cols-lg-2">
      {props.articles.map((m) => (
        <HealthInfoItem key={m.id} article={m} />
      ))}
    </div>
  );
}

export default HealthInfoList;
