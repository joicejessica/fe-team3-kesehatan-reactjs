import HealthInfoList from "./HealthInfoList";
import "./HealthInfo.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "../../utils/constant";

function HealthInfo() {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    const url = `${baseUrl}/article-list`;
    axios.get(url).then((res) => {
      setArticles(res.data.articles);
    });
  }, []);

  return (
    <section id="info-kesehatan" className="d-flex align-items-center">
      <div className="container">
        <h1 className="heading-title">Info Kesehatan</h1>
        <div className="heading-subtitle">Cari artikel kesehatan yang bermanfaat</div>
        <HealthInfoList articles={articles} />
      </div>
    </section>
  );
}

export default HealthInfo;
