import { useState } from "react";
import HealthInfoDetailModal from "./HealthInfoDetailModal";

function HealthInfoItem(props) {
  const [showModal, setShowModal] = useState(false);

  const showModalHandler = () => setShowModal(true);
  const hideModalHandler = () => setShowModal(false);
  const { id, title, image, detailArticle } = props.article;
  const shortText = detailArticle.length < 115 ? detailArticle : detailArticle.substring(0, 115);

  return (
    <>
      <div className="col">
        <div className="article-card">
          <div className="card-image figure">
            <div className="responsive-img" alt="article" style={{ backgroundImage: `url(${image})` }}></div>
          </div>
          <div className="article-properties">
            <h3 className="article-title">{title}</h3>
            <div className="article-summary">{detailArticle}</div>
            <button type="button" className="btn btn-link read-more" onClick={showModalHandler}>
              Baca Selengkapnya
            </button>
          </div>
        </div>
      </div>
      <HealthInfoDetailModal show={showModal} onHide={hideModalHandler} article={props.article} />
    </>
  );
}

export default HealthInfoItem;
