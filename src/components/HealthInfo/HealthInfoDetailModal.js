import Modal from "react-bootstrap/Modal";

function HealthInfoDetailModal(props) {
  const { id, title, image, detailArticle } = props.article;

  return (
    <Modal show={props.show} onHide={props.onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Artikel</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="p-3">
          <div className="question-title-header mb-4">{title}</div>
          <img src={image} alt="Image" className="img-fluid mb-4" />
          <p className="mb-3">{detailArticle}</p>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default HealthInfoDetailModal;
