import { useState, useEffect } from "react";
import LoginModal from "../Login/LoginModal";
import Dropdown from "react-bootstrap/Dropdown";
import ShopModal from "./ShopModal";

function Header(props) {
  const [isSticky, setIsSticky] = useState(false);
  const [showLogin, setShowLogin] = useState(false);
  const [showShop, setShowShop] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [email, setEmail] = useState("");

  useEffect(() => {
    const headerScrolled = () => setIsSticky(window.scrollY > 100);
    document.addEventListener("scroll", headerScrolled);

    const storedLoginInfo = localStorage.getItem("userInfo");
    if (storedLoginInfo) {
      const { email } = JSON.parse(storedLoginInfo);
      setIsLoggedIn(true);
      setEmail(email);
      props.loginChange(true);
    }
  }, []);

  const showLoginHandler = () => setShowLogin(true);
  const hideLoginHandler = () => setShowLogin(false);
  
  const showShopHandler = () => setShowShop(true);
  const hideShopHandler = () => setShowShop(false);


  const logout = () => {
    setIsLoggedIn(false);
    localStorage.clear();
    props.loginChange(false);
  };

  const onLoginSuccessHandler = () => {
    setIsLoggedIn(true);
    setShowLogin(false);
    props.loginChange(true);

    const storedLoginInfo = localStorage.getItem("userInfo");
    if (storedLoginInfo) {
      const { email } = JSON.parse(storedLoginInfo);
      setEmail(email);
    }
  };

  return (
    <>
      <header id="mainHeader" className={`fixed-top d-flex align-items-center ${isSticky ? "header-scrolled" : ""}`}>
        <div className="container">
          <nav className="navbar navbar-expand-lg" id="navbar">
            <a className="navbar-brand" href="/#">
              <img src="./logo.png" alt="logo" />
              <span>Ruang Sehat</span>
            </a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link scrollto active" aria-current="page" href="#banner">
                    Beranda
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link scrollto" href="#tanya-dokter">
                    Tanya Dokter
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link scrollto" href="#info-kesehatan">
                    Info Kesehatan
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link scrollto" href="#cari-rumah-sakit">
                    Cari Rumah Sakit
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link scrollto" href="#cari-dokter">
                    Cari Dokter
                  </a>
                </li>
                <li className="nav-item">
                  <button type="button" className="btn btn-link nav-link scrollto" onClick={showShopHandler}>
                    Toko Sehat
                  </button>
                </li>
                <li>
                  {isLoggedIn ? (
                    <Dropdown>
                      <Dropdown.Toggle className="login" variant="primary" id="dropdown-basic">
                        {email}
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  ) : (
                    <button type="button" className="login btn btn-primary" onClick={showLoginHandler}>
                      Masuk
                    </button>
                  )}
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
      <LoginModal show={showLogin} onHide={hideLoginHandler} onSuccess={onLoginSuccessHandler} />
      <ShopModal show={showShop} onHide={hideShopHandler}/>
    </>
  );
}

export default Header;
