import Modal from "react-bootstrap/Modal";
import Card from "react-bootstrap/Card";
import data from "../../data/shop.json";

function ShopModal(props) {
  return (
    <Modal show={props.show} onHide={props.onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Toko Sehat</Modal.Title>
      </Modal.Header>
      <Modal.Body className="p5">
        <div className="row  row-cols-2 row-cols-lg-3">
          {data.map(({ id, name, link, type }) => (
            <div className="col" key={id}>
              <Card>
                <Card.Img variant="top" src={type === "SHOPEE" ? "./shopee.jpg" : "./tokopedia.png"} />
                <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <a href={link} className="btn btn-primary w-100" target="_blank">
                    Ke Toko
                  </a>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default ShopModal;
