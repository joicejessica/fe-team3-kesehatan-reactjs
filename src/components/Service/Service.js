function Service() {
  return (
    <section id="services" className="d-flex align-items-center">
      <div className="container px-5">
        <div className="row row-cols-1 row-cols-lg-3 justify-content-center services">
          <div className="col d-flex">
            <div className="service-img me-3">
              <img src="./004-doctor-1.png" alt="doctor" className="img-fluid" />
            </div>
            <div className="ms-3">
              <h3>Diskusi Bersama Dokter</h3>
              <p>Diagnosis akurat, bersama dokter hebat</p>
            </div>
          </div>
          <div className="col d-flex">
            <div className="service-img me-3">
              <img src="./005-first-aid-kit.png" alt="first-aid" className="img-fluid" />
            </div>
            <div className="ms-3">
              <h3>Info Kesehatan</h3>
              <p>Diagnosis akurat, bersama dokter hebat</p>
            </div>
          </div>
          <div className="col d-flex">
            <div className="service-img me-3">
              <img src="./001-hospital.png" alt="hospital" className="img-fluid" />
            </div>
            <div className="ms-3">
              <h3>Diskusi Bersama Dokter</h3>
              <p>Diagnosis akurat, bersama dokter hebat</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Service;