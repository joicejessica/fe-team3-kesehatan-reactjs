import { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import AOS from "aos";
import "aos/dist/aos.css";

import "./App.css";
import Header from "./components/Header/Header";
import Banner from "./components/Banner/Banner";
import Service from "./components/Service/Service";
import Discussion from "./components/Discussion/Discussion";
import HealthInfo from "./components/HealthInfo/HealthInfo";
import FindHospital from "./components/FindHospital/FindHospital";
import FindDoctor from "./components/FindDoctor/FindDoctor";
import Footer from "./components/Footer/Footer";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    AOS.init({
      duration: 1000,
      easing: "ease-in-out",
      once: true,
      mirror: false,
    });
  }, []);

  const loginChangeHandler = (isLoggedIn) => {
    setIsLoggedIn(isLoggedIn);
  };

  return (
    <>
      <Header loginChange={loginChangeHandler} />
      <Banner />
      <Service />
      <Discussion isLoggedIn={isLoggedIn} />
      <HealthInfo />
      <FindHospital />
      <FindDoctor />
      <Footer />
    </>
  );
}

export default App;
